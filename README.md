# test-task-data-loader

Hi!

These project consists of:
- src folder: all code is located here
    - lib
        - data-inserter: module to handle data insertion into mongo
        - data-converter: module to convert data into js objects
        - reporter: module to generate and write to file csv report
        - test-helpers: helpers to tester
        - tester: test cases
        - validator: validations
    types: project typings
      
My running environment:
- nodejs 12.13.1
- mongo 3.6.4
- db on port 27017 (url should be 'mongodb://localhost:27017')

First steps:
1. $git clone https://gitlab.com/mezkerei23/test-task-data-loader.git
2. $cd test-task-data-loader
2. $npm i
3. $npm run build

Import data and run tests:
1. node ./build/index.js ../flat_data_bad.csv (or your custom flat data with relative path)
2. check report in src/report.csv
3. exit code 0 - tests are passed, you can continue (CI/CD?)
4. exit code 1 - there are failed test cases

Thanks,
stay healthy
