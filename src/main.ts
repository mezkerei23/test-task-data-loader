import fs from 'fs';
import { Db, MongoClient, ObjectId } from 'mongodb';
import * as path from 'path';
import * as readline from 'readline';
import { recordToPatient, recordToPatientEmail } from './lib/data-converter';
import {
  connect,
  createEmailCollection,
  createPatientsCollection,
  getDb,
  insertEmails,
  insertPatients
} from './lib/data-inserter';
import { writeReport } from './lib/reporter';
import { readEmails, readPatients } from './lib/test-helpers';
import {
  checkConsent,
  checkFirstNames,
  checkPatientEmails,
  checkPatients,
  checkSchedule
} from './lib/tester';
import { validateDateOfBirth } from './lib/validator';
import { Patient, PatientEmail, TestError } from './types/types';

const batchInsertSize: number = 100;

export class Main {
  private client: MongoClient;
  private db: Db;
  private patients: Patient[] = [];
  private patientEmails: PatientEmail[] = [];

  public async prepare(): Promise<void> {
    this.client = await connect();
    this.db = getDb(this.client);
    await createPatientsCollection(this.db);
    return createEmailCollection(this.db);
  }

  public async processData(processArgs: string[]): Promise<void> {
    try {
      console.log('reading data...');
      const fullPath = path.resolve(__dirname, processArgs[2]);
      let errors = [];

      const rl = readline.createInterface({
        crlfDelay: Infinity,
        input: fs.createReadStream(fullPath)
      });

      for await (const line of rl) {
        const values = line.split('|');
        if (values[0] === 'Program Identifier') {
          continue;
        }

        const patient = recordToPatient(values);
        this.patients.push(patient);

        const dateOfBirthValidationError = validateDateOfBirth(patient);
        if (dateOfBirthValidationError) {
          errors.push(dateOfBirthValidationError);
        }

        this.patientEmails.push(recordToPatientEmail(values));

        if (this.patients.length === batchInsertSize) {
          errors = errors.concat(await this.processBatch());
        }
      }

      if (this.patients.length > 0) {
        errors = errors.concat(await this.processBatch());
      }

      console.log(`found ${errors.length} errors...`);

      if (errors.length > 0) {
        console.log(errors);
        await writeReport(errors);

        // fail ci
        process.exit(1);
        return;
      }

      process.exit();
    } catch (err) {
      console.error('main error: ' + err);
    }
  }

  private async processBatch(): Promise<any[]> {
    const patientsIds = await this.insertRecords();
    const errors = this.runTests(patientsIds);
    this.clearRecords();
    return errors;
  }

  private async runTests(patientsIds: string[]): Promise<TestError[]> {
    const ids = patientsIds.map(id => {
      return new ObjectId(id);
    });

    const actualPatients = await readPatients(this.db, ids);
    const actualPatientEmails = await readEmails(this.db, ids);

    return []
      .concat(await checkPatients(actualPatients, this.patients))
      .concat(await checkPatientEmails(actualPatientEmails, this.patientEmails,
        actualPatients))
      .concat(checkFirstNames(actualPatients))
      .concat(checkConsent(actualPatients, actualPatientEmails))
      .concat(checkSchedule(actualPatients, actualPatientEmails));
  }

  private async insertRecords(): Promise<string[]> {
    const patientIds = await insertPatients(this.db, this.patients);
    await insertEmails(this.db, this.patientEmails, patientIds);
    return Object.values(patientIds);
  }

  private clearRecords(): void {
    this.patients = [];
    this.patientEmails = [];
  }
}
