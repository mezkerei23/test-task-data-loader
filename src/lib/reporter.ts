import * as converter from 'convert-array-to-csv';
import fs from 'fs';
import { TestError } from '../types/types';

export async function writeReport(errors: TestError[]): Promise<void> {
  // const csv = errors.map(error => {
  //   return `recordId: ${error.recordId}, message: ${error.message}`;
  // });

  const data = converter.convertArrayToCSV(errors);

  return new Promise((resolve, reject) => {
    fs.writeFile('report.csv', data, (err) => {
      if (err) {
        return reject(err);
      }

      console.log('saved report to report.csv');
      resolve();
    });
  });
}
