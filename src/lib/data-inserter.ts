import { Db, MongoClient } from 'mongodb';
import { Patient, PatientEmail } from '../types/types';

// I thought about adding an additional data layer so I can later change Mongo to
// something else, but it seems like an overkill at this moment.

export async function connect(): Promise<MongoClient> {
  console.log('data-inserter: connecting to db...');
  const url = 'mongodb://localhost:27017'; // todo to args

  const client = new MongoClient(url, { useUnifiedTopology: true });

  await client.connect();
  return client;
}

export function getDb(client: MongoClient): Db {
  const dbName = process.env.DB || 'test-task';
  return client.db(dbName);
}

export async function createPatientsCollection(db: Db): Promise<any> {
  console.log('data-inserter: creating patients collection...');
  await db.createCollection('Patients');
}

export async function createEmailCollection(db: Db): Promise<any> {
  console.log('data-inserter: creating emails collection...');
  await db.createCollection('Email');
}

export async function insertPatients(db: Db,
  patients: Patient[]): Promise<any> {
  console.log('data-inserter: inserting patients...');
  const collection = db.collection('Patients');
  const result = await collection.insertMany(patients);
  return result.insertedIds;
}

export async function insertEmails(db: Db, emails: PatientEmail[],
  insertedPatients: any[]): Promise<any> {
  console.log('data-inserter: inserting emails...');
  const collection = db.collection('Email');

  for (let i = 0; i < emails.length; i++) {
    emails[i].patientId = insertedPatients[i];
  }

  const result = await collection.insertMany(emails);
  return result.insertedIds;
}
