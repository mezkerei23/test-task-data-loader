import {
  ConsentValues,
  DailyEmailType,
  Patient,
  PatientEmail,
  TestError
} from '../types/types';
import { areEmailsEqual, arePatientsEqual } from './test-helpers';

const cadence: DailyEmailType[] = [DailyEmailType.Day1, DailyEmailType.Day2,
  DailyEmailType.Day3, DailyEmailType.Day4];

export function checkPatients(actualPatients: Patient[],
  expectedPatients: Patient[]): TestError[] {

  const errors: TestError[] = [];
  for (let i = 0; i < expectedPatients.length; i++) {
    if (!arePatientsEqual(actualPatients[i], expectedPatients[i])) {
      errors.push({
        message: `patient with id ${actualPatients[i]._id} and member id ${actualPatients[i].memberId} doesn't match with flat data`,
        recordId: actualPatients[i]._id
      });
    }
  }

  return errors;
}

export function checkPatientEmails(actualPatientEmails: PatientEmail[],
  expectedPatientEmails: PatientEmail[],
  actualPatients: Patient[]): TestError[] {

  const errors: TestError[] = [];
  for (let i = 0; i < expectedPatientEmails.length; i++) {
    if (!areEmailsEqual(actualPatientEmails[i], expectedPatientEmails[i],
      actualPatients[i])) {
      errors.push({
        message: `patient emails with id ${actualPatientEmails[i]._id} and member id ${actualPatients[i].memberId} doesn't match with flat data`,
        recordId: actualPatientEmails[i]._id
      });
    }
  }

  return errors;
}

export function checkFirstNames(patients: Patient[]): TestError[] {
  const errors: TestError[] = [];
  patients.forEach(patient => {
    if (!patient.firstName) {
      errors.push({
        message: `patient with id ${patient._id} and member id ${patient.memberId} is missing first name`,
        recordId: patient._id
      });
    }
  });
  return errors;
}

export function checkConsent(patients: Patient[],
  emails: PatientEmail[]): TestError[] {
  const errors: TestError[] = [];
  for (let i = 0; i < patients.length; i++) {
    if (patients[i].consent === ConsentValues.Yes) {
      if (emails[i].emails.length !== cadence.length) {
        errors.push({
          message: `patient emails with id ${emails[i]._id} and member id ${patients[i].memberId} has consent as Y but not enough emails`,
          recordId: emails[i]._id
        });
      }

      emails[i].emails.forEach(email => {
        if (!email.email) {
          errors.push({
            message: `patient emails with id ${emails[i]._id} and member id ${patients[i].memberId} emails value is missing`,
            recordId: emails[i]._id
          });
        }
      });
    }
  }
  return errors;
}

export function checkSchedule(patients: Patient[],
  patientEmails: PatientEmail[]): TestError[] {
  const errors: TestError[] = [];
  for (let i = 0; i < patientEmails.length; i++) {
    if (patients[i].consent === ConsentValues.Yes) {
      for (let j = 0; j < cadence.length; j++) {
        if (!patientEmails[i].emails[j].email) {
          errors.push({
            message: `patient emails with id ${patientEmails[i]._id} and member id ${patients[i].memberId} is missing email value for day ${cadence[j]}`,
            recordId: patientEmails[i]._id
          });
        }

        if (patientEmails[i].emails[j].emailType !== cadence[j]) {
          errors.push({
            message: `patient emails with id ${patientEmails[i]._id} and member id ${patients[i].memberId} schedule is incorrect. Expected ${cadence[i]} got ${patientEmails[i].emails[j].emailType}`,
            recordId: patientEmails[i]._id
          });
        }
      }
    }
  }
  return errors;
}
