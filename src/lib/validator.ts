import moment from 'moment';
import { Patient, TestError } from '../types/types';

export function validateDateOfBirth(patient: Patient): TestError | void {
  if (!moment(patient.dateOfBirth, 'MM/DD/YYYY', true).isValid()) {
    return {
      message: `patient with member id ${patient.memberId} has invalid date of birth ${patient.dateOfBirth}`,
      recordId: patient.memberId
    };
  }
}
