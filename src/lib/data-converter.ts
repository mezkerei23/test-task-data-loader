/* tslint:disable:object-literal-sort-keys */
import {
  ConsentValues,
  DailyEmail,
  DailyEmailType,
  Patient,
  PatientEmail
} from '../types/types';

const cadence: DailyEmailType[] = [DailyEmailType.Day1, DailyEmailType.Day2,
  DailyEmailType.Day3, DailyEmailType.Day4];

// better move indexes to constants
export function recordToPatient(record: string[]): Patient {
  return {
    programIdentifier: record[0],
    dataSource: record[1],
    cardNumber: record[2],
    memberId: record[3],
    firstName: record[4],
    lastName: record[5],
    dateOfBirth: record[6],
    primaryAddress: record[7],
    secondaryAddress: record[8],
    city: record[9],
    state: record[10],
    zipCode: record[11],
    telephoneNumber: record[12],
    consent: record[14],
    mobilePhone: record[15]
  };
}

export function recordToPatientEmail(record: string[]): PatientEmail {
  let emails: DailyEmail[];
  if (record[14] === ConsentValues.Yes) {
    emails = cadence.map(day => {
      return {
        email: record[13],
        emailType: day
      };
    });
  } else {
    emails = [{
      email: record[13],
      emailType: DailyEmailType.Same
    }];
  }
  return {
    emails,
    patientId: '',
  };
}
