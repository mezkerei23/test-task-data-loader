import { Db, ObjectId } from 'mongodb';
import { Patient, PatientEmail } from '../types/types';

export async function readPatients(db: Db,
  insertedPatientsIds: ObjectId[]): Promise<any[]> {

  // both sorted on patients _id, so should have the same order and length
  const result = await db.collection('Patients')
    .find({ _id: { $in: insertedPatientsIds } })
    .sort({ _id: 1 });
  return result.toArray();
}

export async function readEmails(db: Db,
  insertedPatientsIds: ObjectId[]): Promise<any[]> {

  const result = await db.collection('Email')
    .find({ patientId: { $in: insertedPatientsIds } })
    .sort({ patientId: 1 });
  return result.toArray();
}

export function arePatientsEqual(actual: any, expected: Patient): boolean {
  if (!actual || !expected) {
    return false;
  }

  const areMatching = true;
  for (const property in expected) {
    if (property === '_id') {
      continue;
    }
    if (expected[property] !== actual[property]) {
      return false;
    }
  }
  return areMatching;
}

export function areEmailsEqual(actual: any, expected: PatientEmail,
  patient: any): boolean {
  if (!actual || !expected) {
    return false;
  }

  const areMatching = true;
  if (actual.patientId.toString() !== patient._id.toString()) {
    return false;
  }

  for (let i = 0; i < actual.emails.length; i++) {
    if (actual.emails[i].email !== expected.emails[i].email ||
      actual.emails[i].emailType !== expected.emails[i].emailType) {
      return false;
    }
  }

  return areMatching;
}
