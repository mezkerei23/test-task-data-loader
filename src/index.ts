import { Main } from './main';

const start = async () => {
  try {
    console.log('starting...');
    const main = new Main();
    await main.prepare();
    await main.processData(process.argv);
  } catch (err) {
    console.log(err);
  }
};

start();
