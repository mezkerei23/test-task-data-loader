// probably not everyone should be a string, but for the sake of simplicity
// let just leave them as they are
export interface Patient {
  _id?: string,
  programIdentifier: string,
  dataSource: string,
  cardNumber: string,
  memberId: string,
  firstName: string,
  lastName: string,
  dateOfBirth: string,
  primaryAddress: string,
  secondaryAddress: string,
  city: string,
  state: string,
  zipCode: string,
  telephoneNumber: string,
  consent: string,
  mobilePhone: string,
}

export interface DailyEmail {
  readonly emailType: DailyEmailType,
  readonly email: string
}

export interface PatientEmail {
  _id?: string,
  patientId: string,
  emails: DailyEmail[]
}

export interface TestError {
  recordId: string,
  message: string,
}

// todo move from here
export enum DailyEmailType {
  Day1 = 'Day1',
  Day2 = 'Day2',
  Day3 = 'Day3',
  Day4 = 'Day4',
  Same = 'Same',
}

// very important enum
export enum ConsentValues {
  Yes = 'Y',
  No = 'N',
}
